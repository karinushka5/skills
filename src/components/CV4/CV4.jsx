import React, { useState, useMemo } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import HardSkills from "../HardSkills/HardSkills";
import classes from "./CV4.module.scss";

const CV4 = () => {
  const [skills, setSkill] = useState([
    { id: 1, title: "JavaScript" },
    { id: 2, title: "React.Js" },
    { id: 3, title: "Design" },
    { id: 4, title: "UI" },
    { id: 5, title: "UX" },
    { id: 6, title: "Project Planning and Scheduling" },
    { id: 7, title: "Budget Management" },
    { id: 8, title: "Software Requirements" },
  ]);
  const [selectedSkills, setSelectedSkills] = useState([]);
  const [filter, setFilter] = useState({ query: "" });

  const sortedSkills = useMemo(() => {
    return [...skills].sort((a, b) => {
      return a.title.localeCompare(b.title);
    });
  }, [skills]);

  const sortedAndSearchedSkills = useMemo(() => {
    return sortedSkills.filter((skill) =>
      skill.title.toLowerCase().includes(filter.query.toLowerCase())
    );
  }, [filter.query, sortedSkills]);

  return (
    <Formik
      initialValues={{
        language: "",
        languageLevel: "",
        certificate: "",
      }}
      onSubmit={(values) => console.log(JSON.stringify(values, null, 2))}
    >
      <Form>
        <h1 className={classes.CV4__title}>4.Skills</h1>
        <label htmlFor="skills"></label>
        <HardSkills
          selectedSkills={selectedSkills}
          sortedAndSearchedSkills={sortedAndSearchedSkills}
          filter={filter}
          setFilter={setFilter}
        />
        <div className={classes.CV4__language_block}>
          <label htmlFor="language">Language</label>
          <Field className={classes.CV4__language} name="language" as="select">
            <option value="">Choose your language</option>
            <option value="English">English</option>
            <option value="Deutchland">Deutchland</option>
            <option value="France">France</option>
          </Field>
          <Field
            className={classes.CV4__language_level}
            name="languageLevel"
            as="select"
          >
            <option value="">Level</option>
            <option value="A2">A2</option>
            <option value="B1">B1</option>
            <option value="B2">B2</option>
          </Field>
        </div>
        <label htmlFor="sert">Sertificate</label>
        <Field className={classes.CV4__sert} name="sert" type="text"></Field>
        <button
          className={classes.CV4__btn}
          onClick={() =>
            console.log(console.log(JSON.stringify(selectedSkills, null, 2)))
          }
          type="submit"
        >
          Отправить
        </button>
      </Form>
    </Formik>
  );
};

export default CV4;
