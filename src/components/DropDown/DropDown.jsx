import React, { useState } from "react";
import classes from "./DropDown.module.scss";

const DropDown = ({
  visible,
  setVisible,
  selectedSkills,
  setCheaps,
  sortedAndSearchedSkills,
}) => {
  const activeClasses = [classes.dropDown];
  if (visible) {
    activeClasses.push(classes.active);
  }

  const selectSkill = (e) => {
    selectedSkills.push(e.currentTarget.value);
    console.log(selectedSkills);

    if (e.currentTarget.classList.contains(classes.active)) {
      e.currentTarget.classList.remove(classes.active);
    } else {
      e.currentTarget.classList.add(classes.active);
    }
  };

  return (
    <div
      className={activeClasses.join(" ")}
        //  onClick={setVisible(false)}
    >
      <div className={classes.dropDown__block}>
        <p className={classes.dropDown__rec}>Recommended skills</p>
        <div className={classes.dropDown__skills}>
          {sortedAndSearchedSkills.map((skill) => {
            return (
              <input
                type="button"
                value={skill.title}
                key={skill.id}
                className={classes.dropDown__skills_btn}
                onClick={(e) => selectSkill(e)}
              />
            );
          })}
          <button
            type="button"
            onClick={() => {
              setVisible(false);
              setCheaps(true);
            }}
          >
            exit
          </button>
        </div>
      </div>
    </div>
  );
};

export default DropDown;
