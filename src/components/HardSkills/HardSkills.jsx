import React, { useState, useRef } from "react";
import DropDown from "../DropDown/DropDown";
import classes from "./HardSkills.module.scss";

const HardSkills = ({
  selectedSkills,
  sortedAndSearchedSkills,
  filter,
  setFilter,
}) => {
  const [dropDown, setDropDown] = useState(false);
  const [cheaps, setCheaps] = useState(false);

  const skillRefs = useRef([]);

  const classesVisible = [classes.hard__skills_cheaps];
  if (cheaps) {
    classesVisible.push(classes.active);
  }

  const renderSkills = (selectedSkills) => {
    let score = 0;
    const skills =
      cheaps && selectedSkills.slice(0, 4).join("").length < 45
        ? selectedSkills.slice(0, 4).map((item, i) => {
            score++;
            return (
              <div
                className={classes.hard__skills_cheap}
                key={i}
                ref={(el) => (skillRefs.current[i] = el)}
              >
                {item}
              </div>
            );
          })
        : cheaps && selectedSkills.slice(0, 3).join("").length < 45
        ? selectedSkills.slice(0, 3).map((item, i) => {
            score++;
            return (
              <div
                className={classes.hard__skills_cheap}
                key={i}
                ref={(el) => (skillRefs.current[i] = el)}
              >
                {item}
              </div>
            );
          })
        : cheaps && selectedSkills.slice(0, 2).join("").length < 53
        ? selectedSkills.slice(0, 2).map((item, i) => {
            score++;
            return (
              <div
                className={classes.hard__skills_cheap}
                key={i}
                ref={(el) => (skillRefs.current[i] = el)}
              >
                {item}
              </div>
            );
          })
        : cheaps && selectedSkills.slice(0, 1).join("").length < 53
        ? selectedSkills.slice(0, 1).map((item, i) => {
            score++;
            return (
              <div
                className={classes.hard__skills_cheap}
                key={i}
                ref={(el) => (skillRefs.current[i] = el)}
              >
                {item}
              </div>
            );
          })
        : null;
    return (
      <>
        {skills}
        {selectedSkills.length !== score ? (
          <div className={classes.hard__skills_cheap}>
            +{selectedSkills.length - score} Skills
          </div>
        ) : null}
      </>
    );
  };

  const skills = renderSkills(selectedSkills);

  return (
    <div className={classes.hard__skills}>
      <label htmlFor="skills" className={classes.hard__skills_label}>
        Hard Skills
      </label>
      <div className={classes.hard__skills_block}>
        <input
          name="skills"
          className={classes.hard__skills_select}
          type="text"
          placeholder={!cheaps ? 'Enter your skills' : null}
          value={filter.query}
          onChange={(e) => setFilter({ ...filter, query: e.target.value })}
          onFocus={() => {
            setDropDown(true);
            setCheaps(false);
          }}
        />
        <div className={classesVisible.join(" ")}>{skills}</div>
      </div>
      <DropDown
        visible={dropDown}
        setVisible={setDropDown}
        selectedSkills={selectedSkills}
        setCheaps={setCheaps}
        sortedAndSearchedSkills={sortedAndSearchedSkills}
      />
    </div>
  );
};

export default HardSkills;
