import CV4 from "./components/CV4/CV4";

import "./App.css";

function App() {
  return (
    <div className="App">
      <CV4 />
    </div>
  );
}

export default App;
